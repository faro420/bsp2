/*
@author: Mir Farshid Baha
purpose: TI-BSP2
version: v1.0
*/

#include "philos.h"


int get_weights(struct Student* p){
      pthread_mutex_lock( &mutex );
      p->status ='G';
      if(p->check(p)!=0){
        int i;
        for(i=0; i<WEIGHT_COMBINATION;i++){
           supply[i]=supply[i]-(p->combo[i]);    
           }
        p->status ='W';
        display_status();
        }else{
              condition = 1;
         }
     while(condition == 1){
         pthread_cond_wait( &cond, &mutex );
         }
     pthread_mutex_unlock( &mutex );
   return( 0 );
}

int put_weights(struct Student* p){
      pthread_mutex_lock( &mutex );
      p->status ='P';
      int i;
      for(i=0; i<WEIGHT_COMBINATION;i++){
           supply[i]=supply[i]+(p->combo[i]);
           p->combo[i]=0;    
          }
      p->current_weight = 0;
      p->status ='R';
      display_status();
      condition = 0;
      pthread_cond_broadcast( &cond );      
      pthread_mutex_unlock( &mutex );
   return( 0 );
}

void display_status(){
 int i;
 int total_weight = 2*supply[0]+3*supply[1]+5*supply[2];
 for(i=0;i<NUMBER_OF_STUDENTS;i++){
    total_weight=total_weight+students[i].current_weight;
    }

 if(total_weight==TOTAL_WEIGHT){
    struct Student *p;
 for(i=0;i<NUMBER_OF_STUDENTS;i++){
     p = &students[i];
     sprintf(ostr,"%d%s%c:%c:[%d,%d,%d]   ",i,p->name,p->state,p->status,p->combo[0],p->combo[1],p->combo[2]);
     printf("%s",ostr);
    }
    p=NULL;
 sprintf(ostr,"Supply:[%d,%d,%d]\n",supply[0],supply[1],supply[2]);
 printf("%s",ostr);
 }else{
      printf("\nSYNCHRONIZATION FAILED\n");
      }
}

