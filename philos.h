/*
@author: Mir Farshid Baha
purpose: TI-BSP2
version: v1.0
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <assert.h>
#include <string.h>
#include <semaphore.h>

#define TOTAL_WEIGHT 45
#define REST_LOOP 1000000000
#define WORKOUT_LOOP 500000000
#define NUMBER_OF_STUDENTS 5
#define WEIGHT_COMBINATION 3
#define WEIGHTS 3
#define STRLEN 80
#define LEN 10

struct Student{
	char name[10];
	int (*check)(struct Student*);
	int current_weight;
	int combo[WEIGHT_COMBINATION];
        char status;
        char state;
        sem_t t;
};


extern struct Student students[NUMBER_OF_STUDENTS];
extern pthread_mutex_t mutex;
extern pthread_cond_t cond;
extern int condition;
extern int active;
extern sem_t semaphore;
extern pthread_barrier_t barrier;
extern int err;
extern int supply[WEIGHTS];
extern char command[LEN]; //string for command input
extern char ostr[STRLEN];// output string

int get_weights(struct Student*);
int put_weights(struct Student*);

void* philothread(void*args);
void display_status(void);
void init(void);
void destroy(void);
void input_handler(void);

int check0(struct Student*);//Anna
int check1(struct Student*);//Bernd
int check2(struct Student*);//Clara and Dirk
int check3(struct Student*);//Emma
