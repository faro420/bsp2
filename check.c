/*
@author: Mir Farshid Baha
purpose: TI-BSP2
version: v1.0
*/

#include "philos.h"


//Anna
int check0(struct Student* p){
   if(supply[0]>=3){
     p->current_weight = 6;
     p->combo[0]=3;
     p->combo[1]=0;
     p->combo[2]=0;
     return 1;
   }
   if(supply[1]>=2){
     p->current_weight = 6;
     p->combo[0]=0;
     p->combo[1]=2;
     p->combo[2]=0;
     return 1;
   }
  return 0;
}

//Bernd
int check1(struct Student* p){
    if(supply[0]>=4){
      p->current_weight = 8;
      p->combo[0]=4;
      p->combo[1]=0;
      p->combo[2]=0;
      return 1;
    }
    if(supply[0]>=1 && supply[1]>=2){
      p->current_weight = 8;
      p->combo[0]=1;
      p->combo[1]=2;
      p->combo[2]=0;
      return 1;
    }
    if(supply[1]>=1 && supply[2]>=1){
      p->current_weight = 8;
      p->combo[0]=0;
      p->combo[1]=1;
      p->combo[2]=1;
      return 1;
    }
  return 0;
}

//Clara and Dirk
int check2(struct Student* p){
   if(supply[0]>=3 && supply[1]>=2 ){
     p->current_weight = 12;
     p->combo[0]=3;
     p->combo[1]=2;
     p->combo[2]=0;
     return 1;
   }
   if(supply[0]>=2 && supply[1]>=1 && supply[2]>=1 ){
     p->current_weight = 12;
     p->combo[0]=2;
     p->combo[1]=1;
     p->combo[2]=1;
     return 1;
   }
   if(supply[0]>=1 && supply[2]>=2 ){
     p->current_weight = 12;
     p->combo[0]=1;
     p->combo[1]=0;
     p->combo[2]=2;
     return 1;
   }
   if(supply[1]>=4){
     p->current_weight = 12;
     p->combo[0]=0;
     p->combo[1]=4;
     p->combo[2]=0;
     return 1;
   }
return 0;
}

//Emma
int check3(struct Student* p){
   if(supply[0]>=3 && supply[1]>=1 && supply[2]>=1){
     p->current_weight = 14;
     p->combo[0]=3;
     p->combo[1]=1;
     p->combo[2]=1;
     return 1;
   }
   if(supply[1]>=3 && supply[2]>=1){
     p->current_weight = 14;
     p->combo[0]=0;
     p->combo[1]=3;
     p->combo[2]=1;
     return 1;
   }
   if(supply[0]>=2 && supply[2]>=2){
     p->current_weight = 14;
     p->combo[0]=2;
     p->combo[1]=0;
     p->combo[2]=2;
     return 1;
   }
   if(supply[0]>=1 && supply[1]>=4){
     p->current_weight = 14;
     p->combo[0]=1;
     p->combo[1]=4;
     p->combo[2]=0;
     return 1;
   }
return 0;
}
