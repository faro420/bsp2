CC = /usr/bin/gcc
CFLAGS = -pthread -g -Wall -I.
LDFLAGS = -lpthread

prog:philos.c monitor.c init.c check.c philos.h
	$(CC) $(CFLAGS) $(LDFLAGS) -o prog philos.c monitor.c init.c check.c
clean:
	rm -f prog

