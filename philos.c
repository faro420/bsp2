/*
@author: Mir Farshid Baha
purpose: TI-BSP2
version: v1.0
*/

#include "philos.h"

pthread_mutex_t mutex;
pthread_cond_t cond;
pthread_barrier_t barrier;
int condition = 0;
int err;
struct Student students[NUMBER_OF_STUDENTS];
int supply[WEIGHTS]={4,4,5};
char command[LEN];
char ostr[STRLEN];
int active;




int main(void){

init();

int i;
pthread_t threads[NUMBER_OF_STUDENTS];
for(i=0;i<NUMBER_OF_STUDENTS;i++){
    err=pthread_create(&threads[i],NULL,philothread,(void*) &students[i]);
    assert(!err);
}

/*
das ein Funktionsaufruf für den Inputhandler mit einer while-Schleife
als Alternative koennte man die Funktionen als eigene Threadfunktion
realisieren
*/
input_handler();

for(i=NUMBER_OF_STUDENTS-1;i>=0;i--){
   err=pthread_join(threads[i],NULL);
   assert(!err);
   }

destroy();
printf("\nALL THREADS JOINED SUCCESSFULY\n");
exit(EXIT_SUCCESS);
}



void* philothread(void*args){
 struct Student*ptr = ((struct Student*)args);
 int i;
pthread_barrier_wait(&barrier);
 while(active==1){
       while(ptr->status!='W'){
          get_weights(ptr);
          }
      for(i=0;i<WORKOUT_LOOP;i++){
         if(ptr->state == 'b'){
            sem_wait(&(ptr->t));
           }
         if(ptr->state == 'p'){
            ptr->state = 'n';
            i=WORKOUT_LOOP;
            }
        }
           put_weights(ptr);
      for(i=0;i<REST_LOOP;i++){
         if(ptr->state == 'b'){
            sem_wait(&(ptr->t));
           }
         if(ptr->state == 'p'){
            ptr->state = 'n';
            i=REST_LOOP;
            }
        }
     }
   pthread_exit(NULL);
}



void input_handler(){
 int thread_num;
 while(active==1){
     fgets(command,LEN,stdin);
     if(command[0]=='q'||command[0]=='Q'){
       active=0;
       int j;
       for(j=0;j<NUMBER_OF_STUDENTS;j++){
          sem_post(&students[j].t);
          (students+j)->state = 'p';
          }
       }else{
            thread_num = atoi(&command[0]);
            if(thread_num >= NUMBER_OF_STUDENTS){
                printf("%d invalid thread index",thread_num);
               }else{
                    if(command[1] == 'b' || command[1] =='u' || command[1] == 'p'){
                       if(command[1] == 'p'){
                         students[thread_num].state='p';
                         }
                       else if(command[1] == 'b'){
                          students[thread_num].state='b';
                         }else if (command[1] == 'u'){
                                   sem_post(&students[thread_num].t);
                                   students[thread_num].state='n';
                                   }
                         }else{
                              printf("%c unknown command",command[1]);
                              }
             }
           }
     }
}


