/*
@author: Mir Farshid Baha
purpose: TI-BSP2
version: v1.0
*/

#include "philos.h"

void init(){
err=pthread_mutex_init(&mutex, NULL);
assert(!err);
err=pthread_cond_init(&cond, NULL);
assert(!err);
err=pthread_barrier_init(&barrier, NULL, 5);
assert(!err);
active = 1;
int i;
for(i=0;i<NUMBER_OF_STUDENTS;i++){
   sem_init(&students[i].t,0,10);
   (students+i)->current_weight=0;
   (students+i)->status='G';
   (students+i)->state ='n';
   (students+i)->combo[0]=0;
   (students+i)->combo[1]=0;
   (students+i)->combo[2]=0;
   }
students->check = &check0;
sprintf(students->name,"%s","(6)");
(students+1)->check = &check1;
sprintf((students+1)->name,"%s","(8)");
(students+2)->check = &check2;
sprintf((students+2)->name,"%s","(12)");
(students+3)->check = &check2;
sprintf((students+3)->name,"%s","(12)");
(students+4)->check = &check3;
sprintf((students+4)->name,"%s","(14)");
}

void destroy(void){
 int i;
 err=pthread_mutex_destroy(&mutex);
 assert(!err);
 err=pthread_cond_destroy(&cond);
 assert(!err);
 for(i=0;i<NUMBER_OF_STUDENTS;i++){
    err=sem_destroy(&(students+i)->t);
    assert(!err);
    }
}

